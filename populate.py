#!/usr/bin/env python3

import datetime
import sys

import gspread
import requests

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage eg: ./populate.py service_account.json title')
        sys.exit(1)

    accfile, title = sys.argv[1:]
    gc = gspread.service_account(filename=accfile)
    wks = gc.open(title).sheet1

    rows = wks.get_all_values()
    for i, row in enumerate(rows[1:]):
        fields = ['url', 'name', 'rating', 'text', 'review_url',
                  'image_url', 'date']
        data = dict(zip(fields, row))

        date = datetime.datetime.strptime(data['date'], '%B %d, %Y')
        data['date'] = f'{date.year}-{date.month}-{date.day}'

        r = requests.post('http://localhost:8000/reviews/', data=data)
        if r.status_code != requests.codes.created:
            print(i, ':error')
            print(r.json())
        print(i, ':ok')
