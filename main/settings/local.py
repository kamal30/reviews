from .base import *

SECRET_KEY = '%-y#_q1(ab8n5yfxgij*l_yri$rq4mfx)(t&8cp(lo^em@$ycl'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
