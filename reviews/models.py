from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Review(models.Model):
    url = models.URLField()
    name = models.CharField(max_length=200)
    rating = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    text = models.TextField()
    review_url = models.URLField(unique=True)
    image_url = models.URLField()
    date = models.DateField()
