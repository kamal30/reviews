from django.urls import path

from . import views

urlpatterns = [
    path('', views.ReviewListCreate.as_view()),
]
